import { Card, CardHeader, Divider, Grid, Table, TableBody, TableCell, TableRow, Typography } from "@mui/material";


const UserCompany= ({ name, catchPhrase, bs }) => {
  return (
    <Card >
      <CardHeader title="User Company Info" />
      <Divider />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                name
              </Typography>
            </TableCell>
            <TableCell>
              <Typography
                color="textSecondary"
                variant="body2"
              >
                {name}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                catch Phrase
              </Typography>
            </TableCell>
            <TableCell>
              <Typography
                color="textSecondary"
                variant="body2"
              >
                {catchPhrase}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                bs
              </Typography>
            </TableCell>
            <TableCell>
              <Typography
                color="textSecondary"
                variant="body2"
              >
                {bs}
              </Typography>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  );
};



const UserDetails = ({ name, username, email, phone, website } ) => {
  return (
    <Card >
      <CardHeader title="User Details" />
      <Divider />
      <Table>
        <TableBody>
      <TableRow>
        <TableCell>
          <Typography
            color="textPrimary"
            variant="subtitle2"
          >
            Name
          </Typography>
        </TableCell>
        <TableCell>
          <Typography
            color="textSecondary"
            variant="body2"
          >
            {name}
          </Typography>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell>
          <Typography
            color="textPrimary"
            variant="subtitle2"
          >
            Username
          </Typography>
        </TableCell>
        <TableCell>
          <Typography
            color="textSecondary"
            variant="body2"
          >
            {username}
          </Typography>
        </TableCell>
    </TableRow>
      <TableRow>
        <TableCell>
          <Typography
            color="textPrimary"
            variant="subtitle2"
          >
            Website
          </Typography>
        </TableCell>
        <TableCell>
          <Typography
            color="textSecondary"
            variant="body2"
          >
            {website}
          </Typography>
        </TableCell>
      </TableRow>
          <TableRow>
            <TableCell>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                Email
              </Typography>
            </TableCell>
            <TableCell>
              <Typography
                color="textSecondary"
                variant="body2"
              >
                {email}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                Phone
              </Typography>
            </TableCell>
            <TableCell>
              <Typography
                color="textSecondary"
                variant="body2"
              >
                {phone}
              </Typography>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  );
};

const UserAdress= ({ street, city, suite, zipcode }) => {
  return (
    <Card >
      <CardHeader title="User Adress" />
      <Divider />
      <Table>
        <TableBody>
          <TableRow>
            <TableCell>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                Suite
              </Typography>
            </TableCell>
            <TableCell>
              <Typography
                color="textSecondary"
                variant="body2"
              >
                {suite}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                Street
              </Typography>
            </TableCell>
            <TableCell>
              <Typography
                color="textSecondary"
                variant="body2"
              >
                {street}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                City
              </Typography>
            </TableCell>
            <TableCell>
              <Typography
                color="textSecondary"
                variant="body2"
              >
                {city}
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                zipcode
              </Typography>
            </TableCell>
            <TableCell>
              <Typography
                color="textSecondary"
                variant="body2"
              >
                {zipcode}
              </Typography>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  );
};


const User = ({ user = {} }) => (
  <Grid
    container
    spacing={3}
  >
  <Grid
    item
    md={6}
    xs={12}
  >
    <UserDetails {...user}/>
  </Grid>
  <Grid
    item
    md={6}
    xs={12}
  >
    <UserAdress {...user.address}/>
  </Grid>
  <Grid
    item
    md={6}
    xs={12}
  >
    <UserCompany {...user.company}/>
  </Grid>
  </Grid>
);

export default User;

