import { Box } from "@mui/material"
import axios from "axios"
import { useEffect, useState } from "react"
import { useParams } from "react-router"

import { Route, Routes, Navigate } from "react-router-dom"
import User from "./User"

const FetchUser = () => {
  const { id } = useParams()
  const [user, setUser] = useState(null)

  useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then((result) => {
        setUser(result.data)
      })
  }, [id])

  if (!user) return null

  return <User user={user} />
}

const Users = () => (
  <Box sx={{ mt: 3 }}>
    <Routes>
      <Route path="/:id" element={<FetchUser />} />
      <Route path="*" element={<Navigate to="/404" />} />
    </Routes>
  </Box>
)

export default Users
