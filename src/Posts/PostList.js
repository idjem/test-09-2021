import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Pagination } from '@mui/material';
import Post from './Post';
import PostsSkeleton from './PostsSkeleton';
import SelectUser from './SelectUser';

const Posts = ({posts}) => (
  <div>
    {posts.map((post) => <Post key={post.id} {...post}/>)}
  </div>
)

const perPage = 5;

function PostList() {
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);

  const handleChange = (event, value) => {
    setPage(value);
  };
  
  useEffect(() => {
    setLoading(true);
    axios
      .get(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${perPage}${selectedUser ? `&userId=${selectedUser.value}` : ''}`)
      .then(result => {
        setCount(result.headers['x-total-count'] / perPage);
        setPosts(result.data);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [page, selectedUser]);

  const handleUserChange = (user) => {
    setSelectedUser(user);
    setPage(1);
  }

  return (
    <div>
      <SelectUser selectedValue={selectedUser} setSelectedValue={handleUserChange} />
      {loading ?<PostsSkeleton perPage={5} /> : <Posts posts={posts} />}
      <Pagination count={count} page={page} onChange={handleChange} />
    </div>
  );
}

export default PostList;
