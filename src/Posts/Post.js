import { useEffect, useState } from "react";

import { Card, CardActions, CardContent, Collapse, IconButton, Tooltip, Typography } from '@mui/material';
import { Box, styled } from '@mui/system';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import PostComment from './PostComment';
import PostHeader from './PostHeader';
import axios from "axios";
import { red } from "@mui/material/colors";


const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));



const Post = ({title, body, userId, id}) => {
  const [expanded, setExpanded] = useState(false);
  const [like, setLike] = useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ mb: 2 }}>
      <PostHeader
        userId={userId}
        title={title}
      />
      <Box
        sx={{
          pb: 2,
          px: 3
        }}
      >
      <CardContent>
        <Typography variant="body2" color="text.secondary">
          {body}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <Tooltip title={like ? 'Unlike' : 'Like'}>
          <IconButton aria-label="like" onClick={ () => setLike(!like)}>
            <FavoriteIcon fontSize="small" sx={{ color: like ? red['600'] : '' }}/>
          </IconButton>
        </Tooltip>
        <Tooltip title="share">
          <IconButton aria-label="share">
            <ShareIcon fontSize="small"/>
          </IconButton>
        </Tooltip>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <Comments postId={id}/>
      </Collapse>
      </Box>
    </Card>
  )
};


const Comments = ({postId}) => {
  const [comments, setComments] = useState([]);

  useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
      .then(result => {
        setComments(result.data)
      });
  }, [postId]);

  return (<div>
    {comments.map((comment) => (
      <PostComment {...comment} key={comment.id}/>
    ))}
  </div>)
}

export default Post;