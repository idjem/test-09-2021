import { Box } from "@mui/system";
import axios from "axios";
import AsyncSelect from 'react-select/async';



const loadOptions = (inputValue) => {
  return  axios
  .get(`https://jsonplaceholder.typicode.com/users`)
  .then(result => result.data.map((user) => ({value: user.id, label: user.name})))
};

const SelectUser = ({selectedValue, setSelectedValue}) => {
  return (
  <Box sx={{ mb : 2, width : '400px', marginLeft: "auto"}}>
    <AsyncSelect
      cacheOptions
      defaultOptions
      loadOptions={loadOptions}
      onChange={(value) => {setSelectedValue(value)}}
      value={selectedValue}
      isClearable
    />
  </Box>)
}

export default SelectUser;