import { Avatar, CardHeader, Link, Typography } from "@mui/material";

import { Link as RouterLink } from 'react-router-dom';



import axios from "axios";
import { useEffect, useState } from "react";
import { stringToColour } from "../utils";

const PostHeader = ({userId, title}) => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/users/${userId}`)
      .then(result => {
        setUser(result.data)
      });
  }, [userId]);

  if(!user)
    return null;

  return  <CardHeader
      avatar={
        <Avatar sx={{ bgcolor: stringToColour(user.name) }} aria-label="recipe">
          {user.name[0].toUpperCase()}
        </Avatar>
      }
      title={title}
      subheader={(
        <Link
          color="textPrimary"
          component={RouterLink}
          to={`/users/${userId}`}
          variant="subtitle2"
        >
        <Typography
          color="textSecondary"
          variant="caption"
        >
          {user.name}
        </Typography>
      </Link>
    )}
    />
}

export default PostHeader;