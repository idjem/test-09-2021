import React from 'react';
import { Card, CardContent, CardHeader, Skeleton } from '@mui/material';
import { Box } from '@mui/system';

const nLengthArray = (n) => Array(n).fill()

const PostSkeleton = () => (
  <Card sx={{ mb: 2 }}>
    <CardHeader
      avatar={
          <Skeleton animation="wave" variant="circular" width={40} height={40} />
      }
      title={
        <Skeleton
          animation="wave"
          height={10}
          width="80%"
          style={{ marginBottom: 6 }}
        />
        }
        subheader={
          <Skeleton animation="wave" height={10} width="40%" />
        }
      />
    <Box
      sx={{
        pb: 2,
        px: 3
      }}
    >
    <CardContent>
      <Skeleton animation="wave" height={10} style={{ marginBottom: 6 }} />
      <Skeleton animation="wave" height={10} width="80%" />
    </CardContent>
    </Box>
  </Card>
)

const PostsSkeleton = ({perPage = 5}) => {

  return (
    <div>
      {nLengthArray(perPage).map((n, i) => <PostSkeleton key={i}/>)}
    </div>
  );
}

export default PostsSkeleton;
