import { Avatar, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { stringToColour } from "../utils";

const PostComment = ( {body, email, name} ) => {
  return (
    <Box
      sx={{
        display: 'flex',
        mb: 2
      }}
    >
      <Avatar sx={{ bgcolor: stringToColour(name), width: 24, height: 24  }} aria-label="recipe">
        {name[0].toUpperCase()}
      </Avatar>
      <Box
        sx={{
          backgroundColor: 'background.default',
          borderRadius: 1,
          flexGrow: 1,
          ml: 2,
          p: 2
        }}
      >
        <Box
          sx={{
            alignItems: 'center',
            display: 'flex',
            mb: 1
          }}
        >
          <Typography
            variant="subtitle2"
          >
            {name}
          </Typography>
          <Box sx={{ flexGrow: 1 }} />
          <Typography
            color="textSecondary"
            variant="caption"
          >
            {email}
          </Typography>
        </Box>
        <Typography
          color="textPrimary"
          variant="body2"
        >
          {body}
        </Typography>
      </Box>
    </Box>
  );
};

export default PostComment;