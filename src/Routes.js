import PostList from './Posts/PostList';
import User from './User/Index';

const routes = [
  {
    path: 'users/*',
    element: <User />
  },
  {
    path: '/',
    element: <PostList />
  }
];

export default routes;
