import { createTheme, ThemeProvider } from "@mui/material"
import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import { BrowserRouter } from "react-router-dom"

const theme = createTheme({
  components: {
    MuiInputBase: {
      styleOverrides: {
        input: {
          "&::placeholder": {
            opacity: 0.86,
            color: "#42526e"
          }
        }
      }
    }
  },
  palette: {
    action: {
      active: "#6b778c"
    },
    background: {
      default: "#f4f5f7",
      paper: "#ffffff"
    },
    error: {
      contrastText: "#ffffff",
      main: "#f44336"
    },
    mode: "light",
    primary: {
      contrastText: "#ffffff",
      main: "#5664d2"
    },
    success: {
      contrastText: "#ffffff",
      main: "#4caf50"
    },
    text: {
      primary: "#172b4d",
      secondary: "#6b778c"
    },
    warning: {
      contrastText: "#ffffff",
      main: "#ff9800"
    }
  }
})

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
)
