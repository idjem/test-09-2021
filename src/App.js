import { Container } from "@mui/material"
import { styled } from "@mui/system"
import React from "react"
import routes from "./Routes"
import { useRoutes } from "react-router-dom"
import StylesGlobal from "./StylesGlobal"

const DashboardLayoutRoot = styled("div")(({ theme }) => ({
  backgroundColor: theme.palette.background.default,
  height: "100%",
  paddingTop: 64
}))

function App() {
  const routing = useRoutes(routes)

  return (
    <DashboardLayoutRoot>
      <StylesGlobal />
      <Container maxWidth="lg">{routing}</Container>
    </DashboardLayoutRoot>
  )
}

export default App
